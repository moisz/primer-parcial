﻿using System;

namespace primer_parcial
{
    class Program
    {
        
        
        static void Main(string[] args)
        {
            
            int menu,moneda_1 , billetes_de_1000, billetes_de_500, billetes_de_200, billetes_de_100,
            contador_bill_1000 = 18,contador_gene, contador_bill_500 = 19, contador_bill_200 = 23, contador_bill_100 = 50;
            
            do
            {
                Console.WriteLine("\n1- FDP INVERSMENTS");
                Console.WriteLine("2- Otro banco");
                Console.WriteLine("3- Salir");

                Console.Write("\nEscoja una opcion: ");
                menu = int.Parse(Console.ReadLine());

                switch (menu)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("\n----------------Banco FDP INVERSMENTS---------------- \n");
                        Console.Write("Cual es el monto que desea retirar? : ");
                        moneda_1 = int.Parse(Console.ReadLine());
                        
                        //Restrinciones

                        if ( moneda_1 > 20000 )
                        {
                            Console.WriteLine("\nAdvertencia: Lo maximo que puede retirar en este cajero es $20,000\n");
                        } else 
                        {
                            // Distribuidor de billes dependiendo el valor de cada uno y contador de billetes.

                            billetes_de_1000 = (moneda_1 - moneda_1 % 1000)/1000;
                            moneda_1 = moneda_1 % 1000;
                            
                            if (billetes_de_1000 > contador_bill_1000 )
                            {
                                contador_gene = (billetes_de_1000 - contador_bill_1000);
                                billetes_de_1000 =(billetes_de_1000 - contador_gene);
                                contador_gene = (contador_gene * 1000);
                                moneda_1 = (moneda_1 + contador_gene);
                                
                                Console.WriteLine("No hay mas billetes de 1000");
                            }

                            billetes_de_500 = (moneda_1 - moneda_1 % 500)/500;
                            moneda_1 = moneda_1 % 500;

                            if (billetes_de_500 > contador_bill_500 )
                            {
                                contador_gene = (billetes_de_500 - contador_bill_500);
                                billetes_de_500 =(billetes_de_500 - contador_gene);
                                contador_gene = (contador_gene * 500);
                                moneda_1 = (moneda_1 + contador_gene);
                                Console.WriteLine("No hay mas billetes de 500");
                            }


                            billetes_de_200 = (moneda_1 - moneda_1 % 200)/200;
                            moneda_1 = moneda_1 % 200;
                            if (billetes_de_200 > contador_bill_200 )
                            {
                                contador_gene = (billetes_de_200 - contador_bill_200);
                                billetes_de_200 =(billetes_de_200 - contador_gene);
                                contador_gene = (contador_gene * 200);
                                moneda_1 = (moneda_1 + contador_gene);
                                Console.WriteLine("No hay mas billetes de 200");
                            }

                            billetes_de_100 = (moneda_1 - moneda_1 % 100)/100;
                            moneda_1 = moneda_1 % 100;   

                            if (billetes_de_100 > contador_bill_100 )
                            {
                                contador_gene = (billetes_de_100 - contador_bill_100);
                                billetes_de_100 =(billetes_de_1000 - contador_gene);
                                contador_gene = (contador_gene * 100);
                                moneda_1 = (moneda_1 + contador_gene);
                                Console.WriteLine("No hay mas billetes de 100");
                            }
                            
                        
                        // Mostrar por consola la cantidad de billetes que le sera depositada al usuario.

                        Console.WriteLine("La cantidad de billetes de 100 es: {0}", billetes_de_1000);
                        Console.WriteLine("La cantidad de billetes de 500 es: {0}", billetes_de_500);
                        Console.WriteLine("La cantidad de billetes de 200 es: {0}", billetes_de_200);
                        Console.WriteLine("La cantidad de billetes de 100 es: {0}", billetes_de_100);

                        }
                        break;

                    case 2:
                        Console.Clear();
                        Console.WriteLine("\n----------------Otro Banco---------------- \n");
                        Console.Write("Cual es el monto que desea retirar? : ");
                        moneda_1 = int.Parse(Console.ReadLine());
                        
                        //Restrinciones

                        if ( moneda_1 > 10000 )
                        {
                            Console.WriteLine("\nAdvertencia: Lo maximo que puede retirar en este cajero es $10,000\n");
                        } else 
                        {
                            // Distribuidor de billes dependiendo el valor de cada uno.

                            billetes_de_1000 = (moneda_1 - moneda_1 % 1000)/1000;
                            moneda_1 = moneda_1 % 1000;

                            billetes_de_500 = (moneda_1 - moneda_1 % 500)/500;
                            moneda_1 = moneda_1 % 500;

                            billetes_de_200 = (moneda_1 - moneda_1 % 200)/200;
                            moneda_1 = moneda_1 % 200;

                            billetes_de_100 = (moneda_1 - moneda_1 % 100)/100;
                            moneda_1 = moneda_1 % 100;   
                        
                        
                

                        // Mostrar por consola la cantidad de billetes que le sera depositada al usuario.

                        Console.WriteLine("La cantidad de billetes de 1000 es: {0}", billetes_de_1000);
                        Console.WriteLine("La cantidad de billetes de 500 es: {0}", billetes_de_500);
                        Console.WriteLine("La cantidad de billetes de 200 es: {0}", billetes_de_200);
                        Console.WriteLine("La cantidad de billetes de 100 es: {0}\n", billetes_de_100);
                        
                        }
                        break;

                    case 3:
                        Console.Clear();
                        Console.WriteLine("Finalizando Programa...");
                        break;

                    default:
                        Console.Clear();
                        Console.WriteLine("Caracter ingresado no valido");
                        break;

                }

            } while (menu != 3);
        }
    }
}